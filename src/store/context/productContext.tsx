import { ReactNode, createContext, useEffect, useState } from "react";
import { sampleProducts } from "../../product/sampleProductsMock";
import { IProduct } from "../../main/interface";
import { initialState } from "./initialState";
import { IInitialState } from "./interface";

const ProductsContext = createContext<IInitialState>(initialState);

const ProductsProvider = ({ children }: { children: ReactNode }) => {
  const [products, setProducts] = useState<Array<IProduct>>(sampleProducts);
  const [selectedProduct, setSelectedProduct] = useState<IProduct | null>(
    initialState.selectedProduct
  );

  useEffect(() => {
    setProducts(sampleProducts);
  }, []);

  const selectProduct = (id: string) => {
    const findedProduct = products.find((product) => product.id === id);
    setSelectedProduct(findedProduct || null);
  };

  return (
    <ProductsContext.Provider
      value={{ products, selectedProduct, selectProduct }}
    >
      {children}
    </ProductsContext.Provider>
  );
};

export { ProductsProvider, ProductsContext };
