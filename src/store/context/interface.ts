import { IProduct } from "../../main/interface";

export interface IInitialState {
  products: Array<IProduct>,
  selectedProduct: IProduct | null,
  selectProduct: (id: string) => void
}