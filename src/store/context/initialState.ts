import { IInitialState } from './interface';

const initialState: IInitialState = {
  products: [],
  selectProduct: () => undefined,
  selectedProduct: null
};

export { initialState };
