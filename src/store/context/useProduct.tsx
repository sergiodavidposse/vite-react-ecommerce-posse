import { useContext, useState } from "react";
import { ProductsContext } from "./productContext";
import { IProduct } from "../../main/interface";

const useProduct = () => {
  const { products } = useContext(ProductsContext);
  const [selected, setSelected] = useState<IProduct | null>(null);

  const selectProduct = (id: string) => {
    const findedProduct = products.find((product) => product.id === id);
    if (findedProduct) {
      setSelected(findedProduct);
    }
  };

  const findProduct = (id: string) => {
    const findedProduct = products.find((product) => product.id === id);
    if (findedProduct) {
      return findedProduct;
    }
  };

  return { selected, selectProduct, findProduct };
};

export default useProduct;
