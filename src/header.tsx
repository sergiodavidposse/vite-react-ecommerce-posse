import "./styles/main.style.css";
import icon from "./assets/fragancia.png";
import { useCustomNav } from "./shared/utils/usenav";

const Header = () => {
  const { navigateToRoute } = useCustomNav();
  const handleClick = () => {
    navigateToRoute("/");
  };

  return (
    <div className="header">
      <div className="title" onClick={handleClick}>
        Elegant Perfume Fragance
        <img src={icon} />
      </div>
      <div className="sesion">
        <div>Iniciar Sesión</div>
        <div>Registrate!</div>
      </div>
    </div>
  );
};

export default Header;
