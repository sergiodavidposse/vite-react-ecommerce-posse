import axios, { AxiosInstance, AxiosRequestConfig } from 'axios'

export const getAxiosInstance = (): AxiosInstance => {
  // const apiKey = process.env.VITE_KEY_BACKEND || null
  // const headers = apiKey
  //   ? {
  //       'x-api-key': apiKey || '',
  //     }
  //   : null
  const config: AxiosRequestConfig = {
    baseURL: import.meta.env.VITE_BACKEND_BASE_URL,
  }
  return axios.create(config)
}