import ProductCard from "./productCard";
import { sampleProducts } from "./sampleProductsMock";
import "../styles/product-list.style.css";

const ProductsList = () => {
  return (
    <div className="product-list-container">
      {sampleProducts.map((product) => {
        return <ProductCard key={product.id} product={product} />;
      })}
    </div>
  );
};

export default ProductsList;
