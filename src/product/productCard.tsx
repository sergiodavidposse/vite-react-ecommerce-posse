import { IProductCard } from "../main/interface";
import "../styles/product-list.style.css";
import { useNavigate } from "react-router-dom";
import { useContext } from "react";
import { ProductsContext } from "../store/context/productContext";
import { convertPrice } from "../shared/utils/convertPrice";

const ProductCard = (props: IProductCard) => {
  const navigate = useNavigate();
  const { selectProduct } = useContext(ProductsContext);

  const handleClick = (id: string) => {
    selectProduct(id);
    navigate("/product/" + id);
  };

  return (
    <div className="product-card" key={props.product.title}>
      <div className="card-img-container">
        <img className="card-img" src={props.product.thumbnailUrl} />
      </div>
      <h2 className="title-card">{props.product.title}</h2>
      <div className="description">{props.product.description}</div>
      <div>
        {props.product.type === "tangible" ? (
          <button
            onClick={() => handleClick(props.product.id)}
          >{`$ ${convertPrice(props.product.price)}`}</button>
        ) : (
          <button>{`$ ${convertPrice(props.product.price)}`}</button>
        )}
      </div>
    </div>
  );
};

export default ProductCard;
