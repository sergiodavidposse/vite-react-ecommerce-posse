import MultimediaProduct from "./multimediaProduct";
import TangibleProduct from "./tangibleProduct";
import { useContext } from "react";
import { ProductsContext } from "../../store/context/productContext";
import '../../styles/product-detail.style.css';

const ProductDetail = () => {
  const { selectedProduct } = useContext(ProductsContext);

  const ecommerce_type: string =
    import.meta.env.VITE_ECOMMERCE_TYPE || "MULTIMEDIA";

  if (!selectedProduct) return <div>Loading...</div>;

  return (
    <>
      {ecommerce_type === "TANGIBLE" ? (
        <TangibleProduct selectedProduct={selectedProduct} />
      ) : (
        <MultimediaProduct selectedProduct={selectedProduct} />
      )}
    </>
  );
};

export default ProductDetail;
