import { IProduct } from "../../main/interface";

export interface IProductDetailProps {
  selectedProduct: IProduct
}
