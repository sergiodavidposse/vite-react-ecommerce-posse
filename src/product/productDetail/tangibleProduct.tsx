import '../../styles/product-detail.style.css'
import { IProductDetailProps } from "./interface";
import { useNavigate } from "react-router-dom";
import "../../styles/product-detail.style.css";
import { convertPrice } from "../../shared/utils/convertPrice";
import { useEffect } from "react";

const TangibleProduct: React.FC<IProductDetailProps> = (props) => {
  const navigate = useNavigate();

  useEffect(() => {
    window.scrollTo(0, 0)
  }, [])

  const handleClick = async () => {
    navigate("/payment/method");
  };

  return (
    <div className='product-detail-container'>
      <div className="left-side">
        <div className='container-title' key={props.selectedProduct.title}>
          <h2>{props.selectedProduct.title}</h2>
          <h5>{props.selectedProduct.description}</h5>
          <img className='product-detail-img' src={props.selectedProduct.thumbnailUrl} />
        </div>
      </div>
      <div className="right-side">
        <span className="price">{`$ ${convertPrice(
          props.selectedProduct.price
        )}`}</span>

        <p className='product-detail-description'>{props.selectedProduct.fullDescription}</p>
        <button onClick={handleClick}>Select Payment Method</button>
      </div>
    </div>
  );
};

export default TangibleProduct;
