import { IProduct } from "../main/interface";

export const sampleProducts: Array<IProduct> = [
  {
    id: "1",
    title: "Perfume 019919 Argentina 9919",
    price: 141023.23,
    type: "tangible", // image | video | tangible
    url: "", // only multimedia
    thumbnailUrl:
      "https://img.freepik.com/free-photo/front-view-orange-perfume-inside-glass-isolated-white-floor_140725-11649.jpg?t=st=1713377211~exp=1713380811~hmac=9e88fe256080d72c50d467c1639298be62aca21732991a49314cd2f959299afe&w=360",
    description: "yo vull obeir lo manament que·m fa la altesa vostra.",
    fullDescription:
      "- No seria cosa neguna en lo món, senyor, per fort que fos, que yo no manifestàs a la majestat vostra per la molta amor e voluntat que tinch de servir-vos. Per bé que sia cosa de gran dolor, yo vull obeir lo manament que·m fa la altesa vostra. Car yo viu a la sereníssima senyora emperadriu e la excelsa princessa, les II là en taula posades, e sentí un fort e profunde suspir que la senyora emperadriu lançà; pensí que suspirava per aquell que havia parit. En aquell cars, la mia ànima, de pietat sentí dolor inextimable.", //
  },
  {
    id: "2",
    title: "product b",
    price: 132230,
    type: "tangible", // image | video | tangible
    url: "", // only multimedia
    thumbnailUrl:
      "https://juleriaque.vtexassets.com/arquivos/ids/202980/green-generation-edt-8270B014BC0955550823E4F028EC5700.jpg?v=638085750188200000",
    description: "el product a es bueno",
    fullDescription:
      "asd asd asd asd asd asd asd asd asd asd asd asd asdasd asd asd asd asd asd asd asd asd asd asd asd asd asd asd ", //
  },
  {
    id: "3",
    title: "product c",
    price: 100,
    type: "tangible", // image | video | tangible
    url: "", // only multimedia
    thumbnailUrl:
      "https://http2.mlstatic.com/D_NQ_NP_2X_877790-MLU74218925973_012024-F.webp",
    description: "el product a es bueno",
    fullDescription:
      "asd asd asd asd asd asd asd asd asd asd asd asd asdasd asd asd asd asd asd asd asd asd asd asd asd asd asd asd ", //
  },
  {
    id: "4",
    title: "product d",
    price: 100,
    type: "tangible", // image | video | tangible
    url: "", // only multimedia
    thumbnailUrl:
      "https://img.freepik.com/foto-gratis/botella-fragancia-vista-frontal-transparente-piso-blanco_140725-11635.jpg?t=st=1713397554~exp=1713401154~hmac=2433e69fbc6ddcd9176c21b42e7040565a15c1486e97fafd92e2dc684f2f2e2d&w=360",
    description: "el product a es bueno",
    fullDescription:
      "asd asd asd asd asd asd asd asd asd asd asd asd asdasd asd asd asd asd asd asd asd asd asd asd asd asd asd asd ", //
  },
  {
    id: "5",
    title: "product e",
    price: 100,
    type: "tangible", // image | video | tangible
    url: "", // only multimedia
    thumbnailUrl:
      "https://juleriaque.vtexassets.com/arquivos/ids/224268/miss-dior-edp-2BC2BFEDC5A1BB8C09A6490E464212FC.jpg?v=638385989646900000",
    description: "el product a es bueno",
    fullDescription:
      "asd asd asd asd asd asd asd asd asd asd asd asd asdasd asd asd asd asd asd asd asd asd asd asd asd asd asd asd ", //
  },
  {
    id: "6",
    title: "product f",
    price: 100,
    type: "tangible", // image | video | tangible
    url: "", // only multimedia
    thumbnailUrl:
      "https://http2.mlstatic.com/D_NQ_NP_2X_877790-MLU74218925973_012024-F.webp",
    description: "el product a es bueno",
    fullDescription:
      "asd asd asd asd asd asd asd asd asd asd asd asd asdasd asd asd asd asd asd asd asd asd asd asd asd asd asd asd ", //
  },
  {
    id: "7",
    title: "product g",
    price: 100,
    type: "tangible", // image | video | tangible
    url: "", // only multimedia
    thumbnailUrl:
      "https://img.freepik.com/psd-premium/frasco-perfume-humo-rosa_23-2148961303.jpg?w=360",
    description: "el product a es bueno",
    fullDescription:
      "asd asd asd asd asd asd asd asd asd asd asd asd asdasd asd asd asd asd asd asd asd asd asd asd asd asd asd asd ", //
  },
  {
    id: "8",
    title: "product h",
    price: 100,
    type: "tangible", // image | video | tangible
    url: "", // only multimedia
    thumbnailUrl:
      "https://juleriaque.vtexassets.com/arquivos/ids/201516/good-girl-edp-F858FEDBC95CDB92FEC582523C71D3C5.jpg?v=638085739639270000",
    description: "el product a es bueno",
    fullDescription:
      "asd asd asd asd asd asd asd asd asd asd asd asd asdasd asd asd asd asd asd asd asd asd asd asd asd asd asd asd ", //
  },
] as const;