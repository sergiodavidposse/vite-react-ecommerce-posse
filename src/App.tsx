import { BrowserRouter as Router } from "react-router-dom";
import { Routes } from "./routes";
import { ProductsProvider } from "./store/context/productContext";
import Header from "./header";
import Footer from "./footer";
import './styles/main.style.css'
function App() {
  return (
    <ProductsProvider>
      <div>
        <Router>
          <Header />
          <div className="main-container">
            <Routes />
          <Footer />
          </div>
        </Router>
      </div>
    </ProductsProvider>
  );
}

export default App;

