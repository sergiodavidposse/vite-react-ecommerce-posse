import { useCustomNav } from "../../shared/utils/usenav";
import "../../styles/success.style.css";

const Success = () => {
  const { navigateToRoute } = useCustomNav();

  const handleClick = (route: string) => {
    navigateToRoute(route);
  };

  return (
    <div className="main-container-success">
      <div className='legend'>
        <span>Su pago ya ha sido procesado!</span>
        <span>Gracias por su compra.</span>
      </div>
      <button onClick={() => handleClick("/my-purchases")}>
        Ir a mis compras
      </button>
      <button onClick={() => handleClick("/")}>Volver al inicio</button>
    </div>
  );
};

export default Success;
