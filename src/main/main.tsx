import React from "react";
import ProductsList from "../product/productList";
import '../styles/main.style.css'
import { MyCarouselV2 } from "./myCarouselV2";

const imgUrls = [
  "https://static.vecteezy.com/system/resources/previews/015/485/329/non_2x/promo-banner-with-perfume-bottle-in-fire-flame-free-vector.jpg",
  "https://c8.alamy.com/compes/2j327f8/cosmeticos-botella-de-perfume-sobre-superficie-de-agua-salpicada-con-circulos-sobre-fondo-de-cielo-nublado-rosa-paquete-de-tubo-de-vidrio-spray-promo-diseno-de-banner-de-mascup-c-2j327f8.jpg",
  "https://static.vecteezy.com/system/resources/previews/015/680/213/non_2x/perfume-bottles-with-pink-ribbon-and-confetti-free-vector.jpg",
  // "https://t4.ftcdn.net/jpg/02/35/71/91/360_F_235719181_KGtEZLW1kWE5SaeXJku5namLqvNivuZ5.jpg",
  // "https://www.shutterstock.com/shutterstock/photos/2161930777/display_1500/stock-vector-perfume-promo-banner-with-glass-bottles-for-fragrance-pink-ribbon-and-flower-petals-vector-2161930777.jpg",
  // "https://www.vanesacosmetics.com/wp-content/uploads/2022/08/perfume-banner.png",
  // "https://luxuryperfume.com/cdn/shop/files/1_1920x_51ab53b0-0daa-4be8-b762-641671139a87.jpg?v=1684304908"
]

const Main: React.FC = () => {
  return (
    <>
      <MyCarouselV2 imgUrls={imgUrls} />
      <ProductsList />
    </>
  );
};

export default Main;
