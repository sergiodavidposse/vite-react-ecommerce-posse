import { useCustomNav } from "../../shared/utils/usenav";
import "../../styles/error.style.css";

const Error = () => {
  const { navigateToRoute } = useCustomNav();

  const handleClick = (route: string) => {
    navigateToRoute(route);
  };

  return (
    <div className="main-container-error">
      <div className='legend-error'>
        <span>Ha ocurrido un error con su pago!</span>
        <span>Vuelva a intentar mas tarde.</span>
        <span>Disculpe el inconveniente.</span>
      </div>
      <button onClick={() => handleClick("/my-purchases")}>
        Ir a mis compras
      </button>
      <button onClick={() => handleClick("/")}>Volver al inicio</button>
    </div>
  );
};

export default Error;
