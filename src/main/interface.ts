export interface IProduct {
  id: string,
  title: string;
  price: number;
  type: "image" | "video" | "tangible";
  description: string;
  fullDescription?: string;
  thumbnailUrl: string;
  url?: string;
}

export interface IProductCard {
  product: IProduct
}