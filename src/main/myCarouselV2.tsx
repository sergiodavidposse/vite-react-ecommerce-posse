import { useCallback, useEffect, useState } from "react";
import "../styles/carousel.style.css";
export const MyCarouselV2 = ({ imgUrls }: { imgUrls: string[] }) => {
  const [selectedIndex, setSelectedIndex] = useState(0);

  const handleClick = useCallback((direction: string) => {
    if (direction === "prev") {
      const newIndex = selectedIndex === 0 ? imgUrls.length - 1 : selectedIndex - 1;
      setSelectedIndex(newIndex);
    }
    if (direction === "next") {
      const newIndex = selectedIndex === imgUrls.length - 1 ? 0 : selectedIndex + 1;
      setSelectedIndex(newIndex);
    }
  }, [selectedIndex, imgUrls]);

  useEffect(() => {
    const intervalId = setInterval(() => {
      handleClick("next");
    }, 3400);

    return () => clearInterval(intervalId);
  }, [selectedIndex, handleClick]);

  return (
    <div className="carousel-container">
      <div className={`img-container`}>
        {imgUrls.map((image) => {
          return (
            <img
            className="img-container-img"
              key={image}
              style={{ translate: `${-100 * selectedIndex}%` }}
              src={image}
              alt="carousel-image"
            />
          );
        })}
      </div>
      {/* <div className="buttons-container">
        <button onClick={() => handleClick("prev")}>{`<`}</button>
        <button onClick={() => handleClick("next")}>{`>`}</button>
      </div> */}
    </div>
  );
};
