import { useCustomNav } from "../../shared/utils/usenav";
import "../../styles/pending.style.css";

const Pending = () => {
  const { navigateToRoute } = useCustomNav();

  const handleClick = (route: string) => {
    navigateToRoute(route);
  };

  return (
    <div className="main-container-pending">
      <div className='legend-pending'>
        <span>Estamos experimentando demoras con los pagos!</span>
        <span>Su pago esta siendo procesado.</span>
        <span>Puede consultar el estado del mismo desde "Mis compras"</span>
      </div>
      <button onClick={() => handleClick("/my-purchases")}>
        Ir a mis compras
      </button>
      <button onClick={() => handleClick("/")}>Volver al inicio</button>
    </div>
  );
};

export default Pending;