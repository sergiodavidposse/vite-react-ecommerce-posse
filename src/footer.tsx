import "./styles/main.style.css";

const Footer = () => {
  return (
    <div className="footer">
      <span>Desarrollador Sergio Posse ♥ ♥ ♥</span>
    </div>
  );
};

export default Footer;
