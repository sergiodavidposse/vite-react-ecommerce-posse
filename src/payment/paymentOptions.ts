import { MercadoPagoOption } from "./mercadoPagoOption";
import { paypalOption } from "./paypalOption";


export const paymentOptions = () => {
  return [
    {
      id: 1,
      name: "Mercado Pago",
      component: () =>  MercadoPagoOption()
    },
    {
      id: 2,
      name: "Paypal",
      component: () => paypalOption
    }
  ]
}