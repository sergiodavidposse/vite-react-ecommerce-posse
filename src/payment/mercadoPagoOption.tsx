import { Wallet, initMercadoPago } from "@mercadopago/sdk-react";
import { useContext, useEffect, useState } from "react";
import { createPreference } from "../services/mercadopago";
import { ProductsContext } from "../store/context/productContext";
import  "../styles/payment-method.style.css";

// initMercadoPago("APP_USR-7ba5c4f7-61dc-4f45-adb5-8ad7d7f4d2ea");
initMercadoPago(import.meta.env.VITE_MERCADOPAGO_API_KEY);

export const MercadoPagoOption = (): JSX.Element | null => {


  const { selectedProduct } = useContext(ProductsContext);

  const [preferenceId, setPreferenceId] = useState("");

  useEffect(() => {
    const fetchPreference = async () => {
      if (!selectedProduct) return;
      const preferenceData = {
        id: selectedProduct.id,
        name: selectedProduct.title,
        price: selectedProduct.price,
        description: selectedProduct.description,
        thumbnail: selectedProduct.thumbnailUrl,
        currency: "ARS",
      };

      try {
        const response = await createPreference(preferenceData);
        if (response.data.preferenceId) {
          setPreferenceId(response.data.preferenceId);
        }
      } catch (error) {
        console.error("Error creating preference:", error);
      }
    };

    fetchPreference();
  }, [selectedProduct]);

  if (preferenceId === "") {
    return <div className="paypal">Loading...</div>;
  }

  return (
    <div>
      <Wallet
        locale="es-AR"
        initialization={{ preferenceId, redirectMode: "modal" }}
        customization={{ texts: { action: "pay" } }}
      />
    </div>
  );
};
