export interface PaymentOption {
  id: number;
  name: string;
}

export interface PaymentOptionsProps {
  render: (data: { paymentMethods: PaymentOption[] }) => React.ReactNode;
}