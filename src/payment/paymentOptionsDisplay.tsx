import React from "react";
import { MercadoPagoOption } from "./mercadoPagoOption";
import "../styles/payment-method.style.css";

const PaymentOptionsDisplay: React.FC = () => {
  return (
    <div className="payment-container">
      <h2>Elige tu método de pago:</h2>
      <div>
        <MercadoPagoOption />
      </div>
      <div className="paypal">Paypal</div>
    </div>
  );
};

export default PaymentOptionsDisplay;
