import React from "react";
import { Route, Routes as ReactRoutes  } from "react-router-dom";
import { RouteMap } from "./routeMap";

const Routes: React.FC = () => {

  return (
    <ReactRoutes>
      {RouteMap.map((route) => {
        const { path, component: Component, name } = route;
        return <Route path={path} key={name} element={<Component />} />;
      })}
    </ReactRoutes>
  );
};

export { Routes };
