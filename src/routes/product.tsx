import ProductDetail from "../product/productDetail/productDetail";
import { IRouteMap } from "./interface";

export const ProductRoute: IRouteMap[] = [
  {
    name: "product",
    path: "/product/:id",
    component: () => {
      return <ProductDetail />;
    },
  },
];
