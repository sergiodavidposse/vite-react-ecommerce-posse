import Error from "../main/error/error";
import Main from "../main/main";
import Pending from "../main/pending/pending";
import Success from "../main/success/success";
import { IRouteMap } from "./interface";

export const MainRoute: IRouteMap[] = [
  {
    name: "main",
    path: "/",
    component: () => <Main/>
  },
  {
    name: "success",
    path: "/success",
    component: () => {
      return <Success />;
    },
  },
  {
    name: "pending",
    path: "/pending",
    component: () => {
      return <Pending />;
    },
  },
  {
    name: "error",
    path: "/error",
    component: () => {
      return <Error />;
    },
  },
];
