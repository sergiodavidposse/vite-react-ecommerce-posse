import Admin from '../admin/admin'
import { IRouteMap } from './interface'

export const AdminRoute: IRouteMap[] = [
  {
  name: 'admin',
  path: '/admin',
  component: () => {
    return (
        <Admin />
    )
  },
}
]