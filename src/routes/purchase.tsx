import MyPurchases from "../main/purchase/myPurchases";
import { IRouteMap } from "./interface";

export const PurchaseRoute: IRouteMap[] = [
  {
    name: "purchase",
    path: "/my-purchases",
    component: () => {
      return <MyPurchases />;
    },
  },
];
