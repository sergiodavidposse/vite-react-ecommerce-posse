import { AdminRoute } from "./admin";
import { IRouteMap } from "./interface";
import { MainRoute } from "./main";
import { PaymentRoute } from "./payment";
import { ProductRoute } from "./product";
import { PurchaseRoute } from "./purchase";

export const RouteMap: IRouteMap[] = [
  ...MainRoute,
  ...ProductRoute,
  ...AdminRoute,
  ...PaymentRoute,
  ...PurchaseRoute,
];
