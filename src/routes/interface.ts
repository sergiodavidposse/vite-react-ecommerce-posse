export interface IRouteMap {
  name: string
  path: string
  component: React.FC
}