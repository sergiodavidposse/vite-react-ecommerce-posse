import PaymentOptionsDisplay from "../payment/paymentOptionsDisplay";
import { IRouteMap } from "./interface";

export const PaymentRoute: IRouteMap[] = [
  {
    name: "payment",
    path: "/payment/method",
    component: () => {
      return <PaymentOptionsDisplay />;
    },
  },
];
