import { getAxiosInstance } from "../helpers/axiosInstance"

export const createPreference = async ({
  id,
  name,
  price,
  description,
  thumbnail,
  currency
}: {
  id: string
  name: string
  price: number
  description: string
  thumbnail: string
  currency: string
// eslint-disable-next-line @typescript-eslint/no-explicit-any
}): Promise<any> => {
  const body = [{
    id,
    name,
    price,
    description,
    thumbnail,
    currency,
    quantity: 1,
  }]
  return await getAxiosInstance().post('/order/process-order', body)
}